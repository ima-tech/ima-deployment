import os
import os.path
import sys

# python add_apache_listen_port.py port

if len(sys.argv) < 2:
    print('You need a minimum of 1 parameter: port')
    sys.exit(0);

port = sys.argv[1]

apache_ports_file = '/etc/apache2/ports.conf'

with open(apache_ports_file, "a") as file:
    file.write("Listen " + port)
