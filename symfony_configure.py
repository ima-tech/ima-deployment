import os.path
import sys

# Set the variables
website = sys.argv[1]
force_database_install = sys.argv[2]

# Run permissions update on WebsiteVersionFolder
os.system('chmod -R 770 ' + website)
os.system('chown :web -R ' + website)

# Run Symfony setup commands
os.system(website + '/bin/console assets:install ' + website + '/web')

# Set the rights on the .htaccess
os.system('chmod 774 ' + website + '/web/.htaccess')

if force_database_install == True:
    os.system(website + '/bin/console doctrine:schema:update --force')