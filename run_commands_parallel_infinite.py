import os
import time
from subprocess import Popen

commands = [
    ""
]

while True :
    for command in commands:
        # Start a new thread and run the command
        processes = [Popen(cmd, shell=True) for cmd in commands]

    # sleep for 1 second and the commands will be reexecuted
    time.sleep(1)