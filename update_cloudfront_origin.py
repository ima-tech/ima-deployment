import boto3
import sys

client = boto3.client("cloudfront")

if len(sys.argv) < 4:
    print('You need a minimum of 23 parameters: distributionId, originId, originPath')
    sys.exit(0)

distribution_id = sys.argv[1]
origin_id = sys.argv[2]
origin_path = sys.argv[3]

distribution_config = client.get_distribution_config(
    Id=distribution_id
)

etag = distribution_config["ETag"]

config = distribution_config["DistributionConfig"]

origins = config['Origins']['Items']

print(config)

for origin in origins:
    if origin['Id'] == origin_id:
        origin['OriginPath'] = origin_path

response = client.update_distribution(
    DistributionConfig=config,
    Id=distribution_id,
    IfMatch=etag
)

print(response)