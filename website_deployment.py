import os
import os.path
import sys
import zipfile

# Usage example
# python3 website_deployment username password https://api.bitbucket.org/2.0/repositories/ima-tech/XXXXX.git/downloads/XXXXX.zip XXXX.com 1.0.0

tempFolder = '/tmp/'


if len(sys.argv) < 5:
    print('You need 5 parameters: username, password, zipToDownload, websiteToUpdate and version')
    sys.exit(0);

# Set the variables
username = sys.argv[1]
password = sys.argv[2]
zipToDownload = sys.argv[3]
websiteToUpdate = sys.argv[4]
version = sys.argv[5]

authSecret = username + ':' + password

# Create the temp folder
tempFolder = tempFolder +  websiteToUpdate + '_' + version
downloadedFile = tempFolder + '/downloadedFile.zip'
extractedFolder = tempFolder + '/download/'
os.makedirs(tempFolder);

# Retrieve the zip
os.system('curl -L -o ' + downloadedFile + '  --user "' + authSecret + '" ' + zipToDownload)

# Create a new version folder for the website
websiteVersionFolder = '/var/www/' + websiteToUpdate + '/' + version
os.makedirs(websiteVersionFolder);

# Extract the downloaded file to it
zip_ref = zipfile.ZipFile(downloadedFile, 'r')
zip_ref.extractall(websiteVersionFolder)
zip_ref.close()

# Run permissions update on WebsiteVersionFolder
os.system('chmod -R 770 ' + websiteVersionFolder)
os.system('chown :web -R ' + websiteVersionFolder)

# Remove the tmp folder
os.system('rm -Rf ' + tempFolder)
