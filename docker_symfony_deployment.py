import os
import write_to_ini

symfony_env_params_prefix = os.environ["IMATECH_ENV_VAR_SYMFONY_PARAMS_PREFIX"]
symfony_env_filename = os.environ["IMATECH_ENV_VAR_SYMFONY_FILENAME"]

for env_param_name in os.environ:
    env_param_value = os.environ[env_param_name]
    if env_param_name.startsWith(symfony_env_params_prefix):
        # Write this parameter to file
        write_to_ini.write_to_ini("/etc/symfony_parameters/" + symfony_env_filename, "parameters", env_param_name, env_param_value)
