import os
import os.path
import sys

# python AddApacheVirtualHost.py configurationFile ipAddress folderPath [domain]

if len(sys.argv) < 4:
    print('You need a minimum of 3 parameters, configurationFile, ipAddress, folderPath [domain]')
    sys.exit(0);

configurationFile = sys.argv[1]
ipAddress = sys.argv[2]
folderPath = sys.argv[3]
hasDomain = False

if len(sys.argv) == 5:
    domain = sys.argv[4]
    hasDomain = True

template = "<VirtualHost " + ipAddress + ">" + "\n"
template = template + "DocumentRoot \"" + folderPath + "\"\n"
if hasDomain :
    template = template + "ServerName " + domain + "\n"
    template = template + "ServerAlias " + domain + "\n"

template = template + "<Directory \"" + folderPath + "\"> \n"
template = template + "Options Indexes FollowSymLinks Includes ExecCGI \n"
template = template + "AllowOverride All" + "\n"
template = template + "Require all granted"  + "\n"
template = template + "RewriteEngine On" + "\n"
template = template + "RewriteBase /" + "\n"

template = template + "RewriteCond %{HTTP:X-Forwarded-Proto} =http" + "\n"
template = template + "RewriteRule .* https://%{HTTP:Host}%{REQUEST_URI} [L,R=permanent]" + "\n"

template = template + "RewriteRule ^index\.html$ - [L]" + "\n"
template = template + "RewriteCond %{REQUEST_FILENAME} !-f" + "\n"
template = template + "RewriteCond %{REQUEST_FILENAME} !-d" + "\n"
template = template + "RewriteRule . /index.html [L]" + "\n"

template = template + "</Directory>"  + "\n"
template = template + "</VirtualHost>"  + "\n"

apacheFilePath = "/etc/apache2/sites-available/" + configurationFile

with open(apacheFilePath, 'w') as configfile:
    configfile.write(template)
