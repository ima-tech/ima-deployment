import configparser
import os
import os.path

def write_to_ini(iniFile, section, option, value):
    # create the .ini file if it doesn't exist
    if os.path.isfile(iniFile) is False:
        os.makedirs(os.path.dirname(iniFile), exist_ok=True)
        with open(iniFile, "w") as f:
            f.write("")

    config= configparser.ConfigParser()
    config.read(iniFile)

    # verify that the section exist. If not, add it
    if config.has_section(section) is False:
        config.add_section(section)

    # Get the section, option and value to define in the .ini file
    config.set(section, option, value)

    with open(iniFile, 'w') as configfile:
        config.write(configfile)