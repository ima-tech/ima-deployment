import sys
import write_to_ini

if len(sys.argv) < 4:
    print('You need four parameters, iniFile, section, option and value')
    sys.exit(0)

# Open the .ini file
iniFile = sys.argv[1]
section = sys.argv[2]
option = sys.argv[3]
value = sys.argv[4]

write_to_ini.write_to_ini(iniFile, section, option, value)