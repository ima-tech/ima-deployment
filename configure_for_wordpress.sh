#!/bin/sh

a2enmod rewrite
a2enmod ssl
a2enmod php7.3

find /var/www/*/. -type f -exec chmod 644 {} +
find /var/www/*/. -type d -exec chmod 755 {} +
find /var/www/ -iname wp-config.php -exec chmod 600 {} +

chown -R www-data:www-data /var/www/*