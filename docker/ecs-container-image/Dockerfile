FROM ubuntu:16.04

RUN apt-get -y update && apt-get -y install software-properties-common \ 
apt-transport-https \
ca-certificates \
python-software-properties

RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

RUN  apt-get -y update && apt-get install -y -q \
nodejs \
npm \
curl \
nodejs-legacy \
apt-utils \
php7.2 \
php7.1-mcrypt \
php-xml \
php-intl \
php-mysql \
php-curl \
php-gd \
php-mbstring \
php-zip \
python3 \
python-pip \
python3-pip \
git \
zip \
sendmail \
apache2 \
libapache2-mod-php \
sed \
nano \
jq

RUN a2enmod rewrite

# Configure npm
#RUN npm install -g bower
#RUN npm install -g gulp
#RUN npm install -g @angular/cli

# Configure node to the latest stable version
#RUN npm install -g npm && npm -v && npm install -g n
#RUN n stable

#Configure composer
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN addgroup web

RUN usermod -a -G web www-data

#Install AWS-CLI
RUN pip3 install --upgrade pip
ENV PYTHONIOENCODING=UTF-8
RUN pip3 install awscli

# Install AWS boto3 for python usage.
RUN pip3 install boto3

#Install symfony_deployment
RUN mkdir /var/ima-tech
RUN mkdir /var/ima-tech/ima-deployment
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/symfony_deployment.py >> /var/ima-tech/ima-deployment/symfony_deployment.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/write_to_ini.py >> /var/ima-tech/ima-deployment/write_to_ini.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/configure_ini.py >> /var/ima-tech/ima-deployment/configure_ini.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/add_apache_virtual_host.py >> /var/ima-tech/ima-deployment/add_apache_virtual_host.py

RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/add_apache_listen_port.py >> /var/ima-tech/ima-deployment/add_apache_listen_port.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/website_deployment.py >> /var/ima-tech/ima-deployment/website_deployment.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/run_commands_parallel_infinite.py >> /var/ima-tech/ima-deployment/run_commands_parallel_infinite.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/symfony_configure.py >> /var/ima-tech/ima-deployment/symfony_configure.py
RUN curl https://api.bitbucket.org/2.0/repositories/ima-tech/ima-deployment/src/master/add_apache_virtual_host_angular.py >> /var/ima-tech/ima-deployment/add_apache_virtual_host_angular.py

# Install AWS Logs Agent
COPY awslogs /etc/init.d/awslogs

COPY awslogs.conf /tmp/awslogs.conf

RUN mkdir /root/.aws/

COPY credentials /tmp/.aws/credentials

RUN cd /tmp && curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O && python3 ./awslogs-agent-setup.py --region us-east-1 --non-interactive --configfile=/tmp/awslogs.conf

ENV PATH "$PATH:~/.local/bin:/usr/local/bin"
