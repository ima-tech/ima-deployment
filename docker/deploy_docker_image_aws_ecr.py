import os
import os.path
import sys

if len(sys.argv) < 7:
    print('You need five parameters, awsAccessKeyId, awsSecretAccessKey, dockerImageName, version, pathToDockerFile and repository')
    sys.exit(0);

# Open the .ini file
awsAccessKeyId = sys.argv[1]
awsSecretAccessKey = sys.argv[2]
dockerImageName = sys.argv[3]
version = sys.argv[4]
pathToDockerFile = sys.argv[5]
repository = sys.argv[6]

os.system("export AWS_ACCESS_KEY_ID=" + awsAccessKeyId);
os.system("export AWS_SECRET_ACCESS_KEY=" + awsSecretAccessKey);
os.system("export AWS_DEFAULT_REGION=us-east-1");
os.system("export AWS_DEFAULT_OUPUT=json");
os.system("eval $(aws ecr get-login --region us-east-1 --no-include-email)");

os.system("docker build -t " + dockerImageName + ":" + version + " " + pathToDockerFile);

os.system("docker commit $(docker ps -lq) " + dockerImageName);

os.system("docker tag " + dockerImageName + ":" + version + " " + repository + ":" + version)
os.system("docker push " + repository + ":" + version)