#!/bin/sh

LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php

apt-get update && apt-get install -y -q apache2 php7.3 php7.3-mysql php7.3-xml php7.3-simplexml php7.3-gd php7.3-zip php7.3-imagick php7.3-mbstring php7.3-curl

dd if=/dev/zero of=/swapfile bs=100M count=40
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon -s

echo "/swapfile swap swap defaults 0 0" >> /etc/fstab

service apache2 restart